import { Injectable } from '@angular/core';
import {DataService} from "./dataservice.service";
import {Http} from "@angular/http";

@Injectable()
export class QuizService extends DataService {

  constructor(http: Http) {
    super('api/v1/quizzes/', http);
  }

}
