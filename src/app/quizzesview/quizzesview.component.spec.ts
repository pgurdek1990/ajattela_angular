import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizzesviewComponent } from './quizzesview.component';

describe('QuizzesviewComponent', () => {
  let component: QuizzesviewComponent;
  let fixture: ComponentFixture<QuizzesviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizzesviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizzesviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
