import { Component, OnInit } from '@angular/core';
import {QuizService} from "../quiz.service";

@Component({
  selector: 'app-quizzesview',
  templateUrl: './quizzesview.component.html',
  styleUrls: ['./quizzesview.component.css']
})
export class QuizzesviewComponent implements OnInit {

  quizzes: any[];
  constructor(private service: QuizService) { }

  ngOnInit() {
    this.service.getAll().subscribe(
      quizzesFromJson => this.quizzes = quizzesFromJson
    );
  }

}
