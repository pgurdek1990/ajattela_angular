import {Component, OnInit} from '@angular/core';
import {MainviewService} from '../mainview.service';

@Component({
  selector: 'app-mainview',
  templateUrl: './mainview.component.html',
  styleUrls: ['./mainview.component.css']
})
export class MainviewComponent implements OnInit {
  users: any[];

  ngOnInit() {
    this.service.getAll()
      .subscribe(usersFromJson => {
        console.log(usersFromJson);
        // return  this.users = usersFromJson;
      })
    ;
  }

  constructor(private service: MainviewService) {
  }

}
