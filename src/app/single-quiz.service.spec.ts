import { TestBed, inject } from '@angular/core/testing';

import { SingleQuizService } from './single-quiz.service';

describe('SingleQuizService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SingleQuizService]
    });
  });

  it('should be created', inject([SingleQuizService], (service: SingleQuizService) => {
    expect(service).toBeTruthy();
  }));
});
