import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {QuizzesviewComponent} from '../quizzesview/quizzesview.component';
import {MainviewComponent} from '../mainview/mainview.component';
import {SingleQuizService} from "../single-quiz.service";
import {QuizComponent} from "../quiz/quiz.component";

const routes = [
  {
    path: '',
    component: MainviewComponent,
  },
  {
    path: 'quizzesLink',
    component: QuizzesviewComponent,
    children: [
      {
        path: ':id',
        component: QuizComponent
      }
    ]
  },
  //
  // children: [
  // {
  //   path: ':id',
  //   component: null,
  // }
// ]
//     canActivate: [AuthGuard],
  // },
  // {
  //   path: 'login', component: LoginRegisterPageComponent, children: [
  //   {path: '', component: LoginComponent},
  //   {path: 'register', component: RegistrationComponent}
  // ]
  // },
  // {
  //   path: 'register', component: LoginRegisterPageComponent, children: [
  //   {path: '', component: RegistrationComponent},
  //   {path: 'login', component: LoginComponent}
  // ]
  // },
  // {path: '**', redirectTo: '/'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true}),

  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
