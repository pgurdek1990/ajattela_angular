import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
  items: any;

  constructor(protected url: string, protected http: Http) {
  }

  getAll() {
    return this.http.get(this.url)
      .map(response => response.json());
  }

  // getAll() {
  //   return this.authHttp.get(this.url)
  //     .map(response => response.json())
  //     .catch(this.handleError);
  // }
  //
  // create(resource) {
  //   return this.authHttp.post(this.url, resource)
  //     .map(response => response.json())
  //     .catch(this.handleError);
  // }
  //
  // update(id, resource) {
  //   return this.authHttp.put(this.url + '/' + id, resource)
  //     .map(response => response.json())
  //     .catch(this.handleError);
  // }
  //
  // delete(id, item, items) {
  //   this.items = items;
  //   return this.authHttp.delete(this.url + '/' + id, id)
  //     .subscribe(response => {
  //       let index = this.items.indexOf(item);
  //       this.items.splice(index, 1);
  //     });
  //   // .map(response => response)
  //   // .catch(this.handleError);
  // }
  //
  // getResource(id) {
  //   const strid = String(id);
  //   return this.authHttp.get(this.url + '/' + strid)
  //     .map(response => response.json())
  //     .catch(this.handleError);
  // }
  //
  // getResourcePath(path, id) {
  //   const strid = String(id);
  //   return this.authHttp.get(this.url + path + strid)
  //     .map(response => response.json())
  //     .catch(this.handleError);
  // }
  //
  // registerUser(resource) {
  //   return this.http.post(this.url, resource)
  //     .map(response => response)
  //     .catch(this.handleError);
  // }
  //
  // getByQuery(path, query) {
  //   return this.authHttp.get(this.url + path + query)
  //     .map(response => response.json());
  // }
  //
  // getAuthHttp() {
  //   return this.authHttp;
  // }

  // public handleError(error: Response) {
  //   if (error.status === HttpStatus.NOT_FOUND) {
  //     return Observable.throw(new NotFoundError());
  //   } else if (error.status === HttpStatus.CONFLICT) {
  //     return Observable.throw(new ConflictResourceError(error));
  //   } else {
  //     return Observable.throw(new AppError(error));
  //   }
  // }


}
