import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MainviewComponent} from './mainview/mainview.component';
import {DataService} from './dataservice.service';
import {MainviewService} from './mainview.service';
import {HttpModule} from '@angular/http';
import { QuizzesviewComponent } from './quizzesview/quizzesview.component';
import {QuizService} from "./quiz.service";
import {AppRoutingModule} from "./routes/app-routing";
import {MaterializeModule} from "ng2-materialize";
import { QuizComponent } from './quiz/quiz.component';
import {SingleQuizService} from "./single-quiz.service";

@NgModule({
  declarations: [
    AppComponent,
    MainviewComponent,
    QuizzesviewComponent,
    QuizComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    MaterializeModule.forRoot(),
  ],
  providers: [DataService, MainviewService, QuizService, SingleQuizService ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
