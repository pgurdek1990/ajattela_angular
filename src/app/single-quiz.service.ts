import { Injectable } from '@angular/core';
import {DataService} from "./dataservice.service";
import {Http} from '@angular/http';

@Injectable()
export class SingleQuizService extends DataService {

  constructor(http: Http) {
    super('api/v1/quizzes/', http);
  }
  public getById(id) {
    this.url += id;
    this.getAll();
  }
}
