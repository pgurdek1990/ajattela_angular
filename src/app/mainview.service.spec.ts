import {inject, TestBed} from '@angular/core/testing';

import {MainviewService} from './mainview.service';

describe('MainviewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainviewService]
    });
  });

  it('should be created', inject([MainviewService], (service: MainviewService) => {
    expect(service).toBeTruthy();
  }));
});
