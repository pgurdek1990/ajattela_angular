import { AjatellaPage } from './app.po';

describe('ajatella App', () => {
  let page: AjatellaPage;

  beforeEach(() => {
    page = new AjatellaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
